//
//  ViewController.swift
//  LS2HW4_UIView
//
//  Created by Viktor Riga on 05.05.2020.
//  Copyright © 2020 Viktor Riga. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        boxes(count: 10)
    }
    
    func boxes(count: Int) {
        let height = 200
        let width = 200
        let x = 100
        let y = 100
        let offset = 10
        
        for i in 0..<count{
              let box = UIView()
            if i % 2 == 0 {
                box.backgroundColor = UIColor.blue
            } else {
                box.backgroundColor = UIColor.yellow
            }
            print(x/offset+i, y/offset, width-2*i*(x/offset), height-2*i*(y/offset))
            box.frame = CGRect(x: offset+offset*i, y: offset+offset*i , width: width-2*i*(x/offset) , height: height-2*i*(y/offset))
            box.layer.cornerRadius = CGFloat((height-2*i*(y/offset))/2) // rad
            view.addSubview(box)
        }
    }
}

